@extends('main')

@section('title', 'All Posts')

@section('content')
	
	<div class="row">
		<div class="col-md-10">
			<h1>All Posts</h1>	
		</div>
		<div class="col-md-2">
			<a href="{{ route('posts.create') }}" class="btn btn-primary btn-lg btn-block btn-h1-margin">Create Post</a>
		</div>

		<div class="col-md-12">
			<hr />
		</div>
	</div><!-- end of .row -->

	<div class="row">
		<div class="cold-md-12">
			<table class="table">
				<thead>
					<th>#</th>
					<th>Title</th>
					<th>Body</th>
					<th>Create At</th>
					<th></th>
				</thead>
				<tbody>
					@foreach($posts->all() as $post)
						<tr>
							<td>{{ $post->id }}</td>
							<td>{{ $post->title }}</td>
							<td>{{ substr($post->body, 0, 20) }} {{ strlen($post->body) > 20  ? '...' : '' }} </td>
							<td>{{ date('M j, Y h:i a', strtotime($post->created_at) ) }}</td>
							<td><a href=" {{ route('posts.show', $post->id) }} " class="btn btn-default">View</a> <a href=" {{ route('posts.edit', $post->id)}} " class="btn btn-default">Edit</a></td>	
						</tr>					
					@endforeach				
				</tbody>
			</table>

			<div class="text-center">
				{!! $posts->links(); !!}
			</div>
		</div>
		
	</div><!-- end of .row -->
@endsection