<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
 | Public Pages
 */
Route::get('contact', 'PagesController@getContact'); // contact page
Route::get('about', 'PagesController@getAbout'); // about page
Route::get('/', 'PagesController@getIndex'); // home page

// CRUD resource
Route::resource('posts', 'PostController');





