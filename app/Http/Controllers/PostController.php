<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Post; // use post model
use Session; // use session

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // create a variable and store all blog posts in it from the database
        // $posts = Post::all(); // get all post records
        $posts = Post::orderBy('id', 'desc')->paginate(4); // order by the column of the table and the result show the number of post items in this case its 4

        // return a view and pass in all the variable that contains the blog posts
        return view('posts.index')->with('posts', $posts);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate data - array of validation rules
        $this->validate($request, array(
                'title' => 'required|max:255',
                'body' => 'required'
            ));

        // create a new record in the database
        $post = new Post;

        $post->title = $request->title;
        $post->body = $request->body;

        $post->save(); // model function to insert into the database

        Session::flash('success', 'Post have been successfully save!');

        // redirect to another page
        return redirect()->route('posts.show', $post->id); // use the inserted id
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id); // find method only searches the id column
        return view('posts.show')->with('post', $post); // pass data
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // get the data from the database based on the passed id
        $post = Post::find($id);

        // render the data on view
        return view('posts.edit')->with('post', $post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate the data
         $this->validate($request, array(
                'title' => 'required|max:255',
                'body' => 'required'
            ));

        // save the data to the database
         $post = Post::find($id);
         $post->title = $request->input('title');
         $post->body = $request->input('body');

         $post->save();

        // set the flash message
        Session::flash('success', 'This post has successfully saved!');

        // redirect with flash message
        return redirect()->route('posts.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $post = Post::find($id);

        $post->delete();

        Session::flash('success', 'The post was successfully deleted');

        return redirect()->route('posts.index');
    }
}
