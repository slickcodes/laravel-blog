<?php

// this file belongs to this folder
namespace App\Http\Controllers;

use App\Post;

class PagesController extends Controller{

	// actions - get, post, put, delete
	
	// get home page 
	public function getIndex() {

		$posts = Post::select('*')->orderBy('created_at', 'desc')->limit(4)->get();

		return view('pages/welcome')->with('posts', $posts);
	}

	// get about page
	public function getAbout() {
		
		// create variables to store data
		$first = 'Kristoffer';
		$last = 'Bandong';

		$full = $first . ' ' . $last;

		$email = "kris@slickcodes.com";

		// pass an array to the view
		$data = array(
			"fullname" => $full,
			"email" => $email
		);

		return view('pages/about')->with("data", $data); // pass variable 
	}

	// get contact page
	public function getContact() {
		return view('pages/contact');
	}

}

?>